import { Component, Inject, ViewChild, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NGXLogger } from 'ngx-logger';
import * as _ from 'lodash';
import Quagga from 'quagga';

import { SessionManager } from './interface/session';
import { RegistriesService } from './service/registries.service';
import { MatSidenav } from '@angular/material/sidenav';
import { MatIconRegistry, MatIconModule } from '@angular/material/icon';
import { FileService } from './service/file.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Life Registry';
  fileForm: FormGroup;
  scannedCode = "";
  apiKey = "AIzaSyCtblkVNYRZ-OnJBY_Cb2fKwuJZr95mhOo";
  searchID = "006872648842829441782:hhzyw2jyfho";

  configQuagga = {
    inputStream: {
      name: 'Live',
      type: 'LiveStream',
      target: '#inputBarcode',
      constraints: {
        width: { min: 640 },
        height: { min: 480 },
        aspectRatio: { min: 1, max: 100 },
        facingMode: 'environment', // or user
      },
      singleChannel: false // true: only the red color-channel is read
    },
    locator: {
      patchSize: 'medium',
      halfSample: true
    },
    locate: true,
    numOfWorkers: 4,
    decoder: {
      readers: ['code_128_reader']
    }
  };

  @ViewChild('sidenav') public sidenav: MatSidenav;
  @ViewChild('files') files;

  constructor(
    private http: HttpClient,
    private _sm: SessionManager,
    private fb: FormBuilder,
    private _rs: RegistriesService,
    private _fs: FileService,
    private snackBar: MatSnackBar
  ) { }

  public ngOnInit() {
    this.checkUser();

    this.fileForm = this.fb.group({
      scannedImage: null
    });
  }

  public scanCode() {
    let scannedImages = this.files.nativeElement.files;
    this.files.nativeElement.files.value = '';
    let reader = new FileReader();
    if (scannedImages && scannedImages.length > 0) {
      let file = scannedImages[0];
      console.log(file);
      reader.readAsDataURL(file);
      reader.onload = () => {

        Quagga.decodeSingle({
          decoder: {
            readers: ["code_128_reader", "ean_reader", "ean_8_reader", "code_39_reader", "code_39_vin_reader", "codabar_reader",
              "upc_reader", "upc_e_reader", "i2of5_reader", "2of5_reader", "code_93_reader"] // List of active readers
          },
          locate: true, // try to locate the barcode in the image
          src: URL.createObjectURL(file)
        }, (result) => {
          if (result.codeResult) {
            this.scannedCode = result.codeResult.code;
            this._rs.addNewItem({
              title: this.scannedCode,
              url: '',
              description: ''
            });
            // this.http.get('https://www.googleapis.com/customsearch/v1?key=' + this.apiKey + '&cx=' + this.searchID + '&q=' + this.scannedCode)
            this.http.get('https://api.upcitemdb.com/prod/trial/lookup?upc=' + this.scannedCode)
              .subscribe(
                data => {
                  console.log(data);
                },
                error => {
                  console.log(error);
                }
              )
          } else {
            console.log("not detected");
            this.snackBar.open("There was an error scanning your image, please try again", "OK", {
              duration: 2000,
            });
          }
        });
      };
    }
  }

  public openCamera() {
    this.files.nativeElement.click();
  }

  public registryChange(registry) {
    console.log(registry);
    this._rs.newItemForms = [];
    this._rs.newItems = [];
    this.sidenav.toggle();
    this._rs.activeRegistry = registry;
    this._rs.getItems();
  }

  public checkUser() {
    if (this._sm.get('currentUser') == null) {
      setTimeout(() => {
        this.checkUser();
      }, 1000);
    } else {
      this._rs.getRegistries();
      this._rs.getItems();
    }
  }
}
