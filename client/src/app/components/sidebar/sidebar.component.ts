import { Component, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { MatIconRegistry } from '@angular/material/icon';
import * as _ from 'lodash';

import { SessionManager } from '../../interface/session';
import { RegistriesService } from '../../service/registries.service';

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent {
    @Input() registries;
    @Output() registryChange = new EventEmitter<string>();
    @Output() closed = new EventEmitter<boolean>();

    constructor(
        private http: HttpClient,
        private _sm: SessionManager,
        private fb: FormBuilder,
        private _rs: RegistriesService
    ) { }

    public ngOnInit() {
        console.log(this.registries);
    }

    public changeRegistry(registry) {
        console.log(registry);
        this.registryChange.emit(registry);
        this.closed.emit(true);
    }

}