import { Component, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import * as _ from 'lodash';

import { SessionManager } from '../../interface/session';
import { RegistriesService } from '../../service/registries.service';

@Component({
    selector: 'item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.css']
})

export class ItemComponent {
    @Input() item;
    @Input() mode;
    @Input() index;

    urlPreview;
    newItem;
    itemFormGroup;

    constructor(
        private http: HttpClient,
        private _sm: SessionManager,
        private fb: FormBuilder,
        private _rs: RegistriesService
    ) { }

    public ngOnInit() {
        if (this.mode == 'view') {
            this.newItem = false;
            this._rs.getImagePreviews(this.item).subscribe(data => {
                console.log(data);
                this.item.urlPreview = data['image'];
            }, error => {
                console.log(error);
            });

            let itemObj = {
                title: [this.item.title],
                url: [this.item.url],
                description: [this.item.description]
            }
            this._rs.existingItemForms.push(this.fb.group(itemObj));

            this.itemFormGroup = this._rs.existingItemForms[this.index];
        } else if (this.mode == 'edit') { //if we init in edit mode, assume it's a new item
            this.newItem = true;
            this.itemFormGroup = this._rs.newItemForms[this.index];
        }
    }

    public toggleEdit() {
        if (this.mode == 'edit') {
            this.mode = 'view';
        } else if (this.mode == 'view') {
            this.mode = 'edit';
        }
    }
}