import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NGXLogger } from 'ngx-logger';
import * as _ from 'lodash';

import { SessionManager } from '../../interface/session';

@Component({
    selector: 'auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})

export class AuthComponent {
    registerForm: FormGroup;
    loginForm: FormGroup;

    constructor(
        private http: HttpClient,
        private _sm: SessionManager,
        private fb: FormBuilder,
        private snackBar: MatSnackBar
    ) { }

    public ngOnInit() {
        this.loginForm = this.fb.group({
            email: [],
            password: []
        });

        this.registerForm = this.fb.group({
            email: [],
            password: [],
            passwordConfirm: []
        });
    }

    public verifyLogin() {
        console.log('logging in');
        this.http.post('api/verifyLogin', this.loginForm.value)
            .subscribe(
                data => {
                    console.log(data);
                    this._sm.save("currentUser", data);
                },
                error => {
                    if (error.status === 401) {
                        this.snackBar.open("Either the username or password is incorrect!", "Ok", { duration: 3000 });
                    }
                    console.log(error);
                }
            )
    }

    public registerUser() {
        var registerForm = this.registerForm.value;
        if (registerForm.password == registerForm.passwordConfirm) {
            this.http.post('api/registerUser', this.registerForm.value)
                .subscribe(
                    data => {
                        if (data['errmsg']) {
                            this.snackBar.open(data['errmsg']);
                        } else {
                            this.snackBar.open("A verification email has been sent, please check the email you entered", "Ok", { duration: 10000 });
                        }
                    },
                    error => {
                        this.snackBar.open("An error occured during registration", "Ok", { duration: 5000 });
                        console.log(error);
                    }
                )
        } else {
            this.snackBar.open("The passwords you entered don't match!", "Ok", { duration: 5000 });
            console.log('password mismatch');
        }
    }
}