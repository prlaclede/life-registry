import { Injectable, Inject } from '@angular/core';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Injectable()
export class SessionManager {
    constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService) { }

    save(key, val): void {
        this.storage.set(key, val);
        console.log(this.storage);
    }

    get(key) {
        return this.storage.get(key);
    }
}