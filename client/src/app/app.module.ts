import { BrowserModule } from '@angular/platform-browser';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { StorageServiceModule } from 'angular-webstorage-service';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ItemComponent } from './components/item/item.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AuthComponent } from './components/auth/auth.component';
import { MaterialModule } from './material/material.module';

import { SessionManager } from './interface/session';
import { RegistriesService } from './service/registries.service';
import { FileService } from './service/file.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    ItemComponent,
    SidebarComponent
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    BrowserModule,
    MaterialModule,
    StorageServiceModule,
    LoggerModule.forRoot({ serverLoggingUrl: '../logs', level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR })
  ],
  providers: [
    SessionManager,
    RegistriesService,
    FileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }