import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class FileService {
    _url: string;
    constructor(private http: HttpClient) { }

    uploadFile(data: any): Observable<{}> {
        this._url = 'api/saveImage';
        return this.http.post(this._url, data);
    }
}
