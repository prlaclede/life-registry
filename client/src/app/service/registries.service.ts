import { Component, Injectable, ViewChild, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import * as _ from 'lodash';

import { SessionManager } from '../interface/session';

@Injectable()
export class RegistriesService {

    activeRegistry = "wish list";
    registries;
    registryItems;
    newItems = [];
    newItemForms = [];
    existingItemForms = [];
    imagePreviews = {};

    constructor(
        private http: HttpClient,
        private fb: FormBuilder,
        private _sm: SessionManager
    ) { }

    public getRegistries() {
        this.http.post('api/getAllLists', { userID: this._sm.get('currentUser')['_id'] })
            .subscribe(
                data => {
                    console.log(data);
                    this.registries = data;
                },
                error => {
                    console.log(error);
                }
            )
    }

    public getItems() {
        console.log('getting items');
        this.http.post('api/getByReg', { registry: this.activeRegistry, userID: this._sm.get('currentUser')['_id'] })
            .subscribe(
                itemData => {
                    console.log(itemData);
                    this.registryItems = itemData;
                    this.existingItemForms = [];
                },
                error => {
                    console.log(error);
                }
            )
    }

    public addNewItem(initItem?) {
        let newItemObj = {
            title: [],
            url: [],
            description: []
        }

        if (initItem) {
            newItemObj.title = initItem.title;
            newItemObj.url = initItem.url;
            newItemObj.description = initItem.description;
        }

        this.newItemForms.push(this.fb.group(newItemObj));

        this.newItems.push({
            title: '',
            url: '',
            description: ''
        })
    }

    public goToItem(i) {
        var indexedItem = this.registryItems[i];
        window.open(indexedItem.url, '_blank');
    }

    public saveItem(index, newItem, itemFormGroup) {
        var itemObj = itemFormGroup.value;
        itemObj.userID = this._sm.get('currentUser')['_id'];
        itemObj.registry = this.activeRegistry;
        console.log(itemObj);
        if (newItem) {
            this.http.post('api/saveItem', itemObj)
                .subscribe(data => {
                    console.log(data);
                    this.newItemForms.splice(index, 1);
                    this.newItems.splice(index, 1);
                    this.getItems();
                }, error => {
                    console.log(error);
                })
        } else {
            itemObj['_id'] = this.registryItems[index]._id;
            this.http.post('api/updateItem', itemObj)
                .subscribe(data => {
                    console.log(data);
                    this.getItems();
                }, error => {
                    console.log(error);
                })
        }

    }

    deleteItem(i, mode, newItem) {
        if (mode == 'view' || (mode == 'edit' && !newItem)) {
            this.deleteExistingItem(i);
        } else if (mode == 'edit' && newItem) {
            this.deleteNewItem(i);
        }
    }

    public deleteNewItem(i) {
        this.newItemForms.splice(i, 1);
        this.newItems.splice(i, 1);
    }

    public deleteExistingItem(i) {
        var body = {};
        var indexedItem = this.registryItems[i];
        body['_id'] = indexedItem['_id'];
        this.http.post('api/deleteItem', body)
            .subscribe(data => {
                this.getItems();
            }, error => {
                console.log(error);
            })
    }

    public getImagePreviews(item) {
        var body = {};
        body['url'] = item.url;
        return this.http.post('api/getURLPreview', body);
    }

    public getNewItemPreview(index, item) {
        let newitemControls = this.newItemForms[index].controls;
        let url = newitemControls.url.value;
        var body = {};
        body['url'] = url;
        this.http.post('api/getURLPreview', body).subscribe(data => {
            item.urlPreview = data['image'];
            if (data['description']) {
                this.newItemForms[index].patchValue({ 'description': data['description'] });
            }
            if (data['title']) {
                this.newItemForms[index].patchValue({ 'title': data['title'] });
            }
        }, error => {
            console.log(error);
        });
    }
}
