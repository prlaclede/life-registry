import * as mongoose from 'mongoose';

export class Users {
    constructor() { }

    email: String;
    password: String;
    created: Date;

    public setEmail(email) {
        this.email = email;
    }

    public setPassword(password) {
        this.password = password;
    }

    public setCreated(created) {
        this.created = created;
    }


    private userSchema = new mongoose.Schema({
        email: String,
        password: String,
        created: Date
    });

    public UserModel = mongoose.model('Users', this.userSchema, 'Users');
}