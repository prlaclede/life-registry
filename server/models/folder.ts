import * as mongoose from 'mongoose';

const folderSchema = new mongoose.Schema({
    name: String,
    description: String,
    created: Date
});

const Folder = mongoose.model('Folder', folderSchema);

export default Folder;