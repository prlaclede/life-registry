import * as Folder from './folder';
import * as mongoose from 'mongoose';

const listSchema = new mongoose.Schema({
    name: String,
    description: String,
    created: Date,
    folder: [Folder]
});

const List = mongoose.model('List', listSchema);

export default List;