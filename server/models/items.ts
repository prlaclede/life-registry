import * as mongoose from 'mongoose';

export class Items {
    constructor() { }

    title: String;
    url: String;
    description: String;
    registry: String;
    userID: String;

    public setTitle(title) {
        this.title = title;
    }

    public setURL(url) {
        this.url = url;
    }

    public setDescription(description) {
        this.description = description;
    }

    public setRegistry(registry) {
        this.registry = registry;
    }

    public setUserID(userID) {
        this.userID = userID;
    }

    private itemSchema = new mongoose.Schema({
        title: String,
        description: String,
        url: String,
        registry: String,
        userID: String
    });

    public ItemModel = mongoose.model('Items', this.itemSchema, 'Items');
}
