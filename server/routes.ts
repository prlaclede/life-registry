import * as express from 'express';
import * as request from 'request';
import * as cheerio from 'cheerio';

import * as cors from "cors";

import ItemCtrl from './controllers/item.mao';
import UserCtrl from './controllers/user.mao';

export default function setRoutes(app, db) {

  const router = express.Router();

  //options for cors midddleware
  const options: cors.CorsOptions = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: "http://localhost:4200/",
    preflightContinue: false
  };

  router.use(cors(options));

  const itemCtrl = new ItemCtrl();
  const userCtrl = new UserCtrl();

  router.route('/testRouter').get(function () { console.log('router active!'); });

  router.route('/getByReg').post(itemCtrl.getAllByReg);
  router.route('/getAllLists').post(itemCtrl.getAllLists);
  router.route('/saveItem').post(itemCtrl.saveItem);
  router.route('/deleteItem').post(itemCtrl.deleteItem);
  router.route('/updateItem').post(itemCtrl.updateItem);

  router.route('/verifyLogin').post(userCtrl.verifyLogin);
  router.route('/registerUser').post(userCtrl.registerUser);

  router.route('/getURLPreview').post(function (req, res) {
    const url = req.body.url;
    req.header('Access-Control-Allow-Origin', true);
    request(url, function (error, response, html) {
      if (error) {
        res.status(200).json({
          error: error,
          html: html,
          status: response && response.statusCode
        });
      } else {
        const $ = cheerio.load(html.toString());
        res.status(200).json({
          title: $('meta[property="og:title"]').attr('content'),
          description: $('meta[property="og:description"]').attr('content'),
          image: $('meta[property="og:image"]').attr('content'),
          html: html,
          status: response && response.statusCode
        });
      }
    });
  });

  router.options("*", cors(options));

  // Apply the routes to our application with the prefix /api
  app.use('/api', router);

}