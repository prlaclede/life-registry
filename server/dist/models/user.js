"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var Users = /** @class */ (function () {
    function Users() {
        this.userSchema = new mongoose.Schema({
            email: String,
            password: String,
            created: Date
        });
        this.UserModel = mongoose.model('Users', this.userSchema);
    }
    Users.prototype.setEmail = function (email) {
        this.email = email;
    };
    Users.prototype.setPassword = function (password) {
        this.password = password;
    };
    Users.prototype.setCreated = function (created) {
        this.created = created;
    };
    return Users;
}());
exports.Users = Users;
