"use strict";
exports.__esModule = true;
var Folder = require("./folder");
var mongoose = require("mongoose");
var listSchema = new mongoose.Schema({
    name: String,
    description: String,
    created: Date,
    folder: [Folder]
});
var List = mongoose.model('List', listSchema);
exports["default"] = List;
