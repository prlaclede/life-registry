"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var Items = new mongoose.Schema({
    title: String,
    description: String,
    url: String,
    registry: String
});
var Item = mongoose.model('Item', Items);
exports["default"] = Item;
