"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var folderSchema = new mongoose.Schema({
    name: String,
    description: String,
    created: Date
});
var Folder = mongoose.model('Folder', folderSchema);
exports["default"] = Folder;
