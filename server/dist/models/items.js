"use strict";
exports.__esModule = true;
var mongoose = require("mongoose");
var Items = /** @class */ (function () {
    function Items() {
        this.itemSchema = new mongoose.Schema({
            title: String,
            description: String,
            url: String,
            registry: String,
            userID: String
        });
        this.ItemModel = mongoose.model('Items', this.itemSchema, 'Items');
    }
    Items.prototype.setTitle = function (title) {
        this.title = title;
    };
    Items.prototype.setURL = function (url) {
        this.url = url;
    };
    Items.prototype.setDescription = function (description) {
        this.description = description;
    };
    Items.prototype.setRegistry = function (registry) {
        this.registry = registry;
    };
    Items.prototype.setUserID = function (userID) {
        this.userID = userID;
    };
    return Items;
}());
exports.Items = Items;
