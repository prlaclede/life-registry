"use strict";
exports.__esModule = true;
var express = require("express");
var request = require("request");
var cheerio = require("cheerio");
var cors = require("cors");
var item_mao_1 = require("./controllers/item.mao");
var user_mao_1 = require("./controllers/user.mao");
function setRoutes(app, db) {
    var router = express.Router();
    //options for cors midddleware
    var options = {
        allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
        credentials: true,
        methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
        origin: "http://localhost:4200/",
        preflightContinue: false
    };
    router.use(cors(options));
    var itemCtrl = new item_mao_1["default"]();
    var userCtrl = new user_mao_1["default"]();
    router.route('/testRouter').get(function () { console.log('router active!'); });
    router.route('/getByReg').post(itemCtrl.getAllByReg);
    router.route('/getAllLists').post(itemCtrl.getAllLists);
    router.route('/saveItem').post(itemCtrl.saveItem);
    router.route('/deleteItem').post(itemCtrl.deleteItem);
    router.route('/updateItem').post(itemCtrl.updateItem);
    router.route('/verifyLogin').post(userCtrl.verifyLogin);
    router.route('/registerUser').post(userCtrl.registerUser);
    router.route('/getURLPreview').post(function (req, res) {
        var url = req.body.url;
        req.header('Access-Control-Allow-Origin', true);
        request(url, function (error, response, html) {
            if (error) {
                res.status(200).json({
                    error: error,
                    html: html,
                    status: response && response.statusCode
                });
            }
            else {
                var $ = cheerio.load(html.toString());
                res.status(200).json({
                    title: $('meta[property="og:title"]').attr('content'),
                    description: $('meta[property="og:description"]').attr('content'),
                    image: $('meta[property="og:image"]').attr('content'),
                    html: html,
                    status: response && response.statusCode
                });
            }
        });
    });
    router.options("*", cors(options));
    // Apply the routes to our application with the prefix /api
    app.use('/api', router);
}
exports["default"] = setRoutes;
