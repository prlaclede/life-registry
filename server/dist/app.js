"use strict";
exports.__esModule = true;
var express = require("express");
var mongoose = require("mongoose");
var logger = require("morgan");
var bodyParser = require("body-parser");
var q = require("q");
var routes_1 = require("./routes");
var app = express();
exports.app = app;
//options for cors midddleware
// const options:cors.CorsOptions = {
//   allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
//   credentials: true,
//   methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
//   origin: "/api",
//   preflightContinue: false
// };
// app.use(cors(options));
// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });
app.use(express.json());
app.listen(3000, function (err) {
    if (err) {
        return console.log(err);
    }
    return console.log('My Express App listening on port 3000');
});
//mount logger
this.app.use(logger("dev"));
app.use(express.urlencoded({ extended: false }));
//mount json form parser
this.app.use(bodyParser.json());
//mount query string parser
this.app.use(bodyParser.urlencoded({
    extended: true
}));
global.Promise = q.Promise;
mongoose.Promise = global.Promise;
// let mongodbURI = 'mongodb+srv://lifereg:l1f3r3g@cluster0-euftp.azure.mongodb.net/test?retryWrites=true&w=majority';
var mongodbURI = 'mongodb+srv://LifeRegApp:Y3ahruCaGmZAl4Yh@cluster0-euftp.azure.mongodb.net/LifeReg?retryWrites=true&w=majority';
mongoose.connect(mongodbURI, { useNewUrlParser: true })
    .then(function (db) {
    console.log('Connected to MongoDB');
    routes_1["default"](app, db);
})["catch"](function (err) { return console.error(err); });
