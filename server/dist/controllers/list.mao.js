"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var list_1 = require("../models/list");
var base_mao_1 = require("./base.mao");
var FolderMao = /** @class */ (function (_super) {
    __extends(FolderMao, _super);
    function FolderMao() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = list_1["default"];
        return _this;
    }
    return FolderMao;
}(base_mao_1["default"]));
exports["default"] = FolderMao;
