"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var users_1 = require("../models/users");
var base_mao_1 = require("./base.mao");
var bcrypt = require("bcrypt");
var nodemailer = require('nodemailer');
var UserMAO = /** @class */ (function (_super) {
    __extends(UserMAO, _super);
    function UserMAO() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.user = new users_1.Users();
        _this.model = _this.user.UserModel;
        // Verify user login attempt
        _this.verifyLogin = function (req, res) {
            var queryItem = { email: req.body.email };
            _this.model.findOne(queryItem, function (err, user) {
                if (err) {
                    res.status(500).json(err);
                }
                else if (user) { //if we found a user
                    // check the pass
                    if (!bcrypt.compareSync(req.body.password, user.password)) {
                        res.status(401).json(err);
                    }
                    else {
                        user.password = undefined;
                        res.status(200).json(user);
                    }
                }
                else { //if the is no user
                    res.status(401).json(err);
                }
            });
        };
        _this.registerUser = function (req, res) {
            if (req.body.password === req.body.passwordConfirm) {
                var newUser = new _this.user.UserModel();
                newUser.email = req.body.email;
                newUser.password = bcrypt.hashSync(req.body.password, 10);
                newUser.created = new Date();
                newUser.active = false;
                // nodemailer.createTestAccount((err, account) => {
                //     // create reusable transporter object using the default SMTP transport
                //     let transporter = nodemailer.createTransport({
                //         host: 'smtp.ethereal.email',
                //         port: 587,
                //         secure: false, // true for 465, false for other ports
                //         auth: {
                //             user: account.user, // generated ethereal user
                //             pass: account.pass  // generated ethereal password
                //         }
                //     });
                //     console.log('sending mail');
                //     let info = transporter.sendMail({
                //         from: '"Life Registry" <LifeReg@noreply.com>', // sender address
                //         to: newUser.email, // list of receivers
                //         subject: "Please validate your email for Life Registry.", // Subject line
                //         text: "Hello! Please validate your email for Life Registry", // plain text body
                //         html: "<b>Hello! Please validate your email for Life Registry</b>" // html body
                //     });
                // });
                newUser.save(function (data) {
                    res.status(200).json(data);
                });
            }
        };
        return _this;
    }
    return UserMAO;
}(base_mao_1["default"]));
exports["default"] = UserMAO;
