"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var items_1 = require("../models/items");
var base_mao_1 = require("./base.mao");
var ItemMao = /** @class */ (function (_super) {
    __extends(ItemMao, _super);
    function ItemMao() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.item = new items_1.Items();
        _this.model = _this.item.ItemModel;
        _this.getAllByReg = function (req, res) {
            console.log(req.body.regName);
            var queryItem = { registry: req.body.registry, userID: req.body.userID };
            _this.model.find(queryItem, function (err, items) {
                if (err) {
                    console.log(err);
                    res.status(500).json(err);
                }
                else {
                    res.status(200).json(items);
                }
            });
        };
        _this.getAllLists = function (req, res) {
            console.log(req.body.regName);
            var queryItem = { userID: req.body.userID };
            _this.model.distinct('registry', queryItem, function (err, items) {
                if (err) {
                    console.log(err);
                    res.status(500).json(err);
                }
                else {
                    res.status(200).json(items);
                }
            });
        };
        _this.saveItem = function (req, res) {
            var itemParams = req.body;
            var newItem = new _this.item.ItemModel();
            newItem.title = itemParams.title;
            newItem.url = itemParams.url;
            newItem.description = itemParams.description;
            newItem.registry = itemParams.registry;
            newItem.userID = itemParams.userID;
            newItem.save(function (err, item) {
                console.log('saved');
                console.log(item);
                if (err) {
                    return res.status(200).json(err);
                }
                else if (item) {
                    return res.status(200).json(item);
                }
            });
        };
        _this.updateItem = function (req, res) {
            var itemParams = req.body;
            console.log(itemParams);
            _this.model.update({ _id: itemParams._id }, itemParams, function (err, item) {
                console.log('updated');
                console.log(item);
                if (err) {
                    return res.status(200).json(err);
                }
                else {
                    return res.status(200).json(item);
                }
            });
        };
        _this.deleteItem = function (req, res) {
            console.log(req.body);
            var queryItem = { _id: req.body._id };
            _this.model.findOneAndDelete(queryItem, function (err, item) {
                console.log('removed');
                console.log(item);
                if (err) {
                    return res.status(200).json(err);
                }
                else {
                    return res.status(200).json(item);
                }
            });
        };
        return _this;
    }
    return ItemMao;
}(base_mao_1["default"]));
exports["default"] = ItemMao;
