"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var folder_1 = require("../models/folder");
var base_mao_1 = require("./base.mao");
var ListMao = /** @class */ (function (_super) {
    __extends(ListMao, _super);
    function ListMao() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.model = folder_1["default"];
        return _this;
    }
    return ListMao;
}(base_mao_1["default"]));
exports["default"] = ListMao;
