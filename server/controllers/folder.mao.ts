import Folder from '../models/folder';
import BaseMao from './base.mao';

export default class ListMao extends BaseMao {
    model = Folder;
}