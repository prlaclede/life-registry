import List from '../models/list';
import BaseMao from './base.mao';

export default class FolderMao extends BaseMao {
    model = List;
}