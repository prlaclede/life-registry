import { Users } from '../models/users';
import BaseMao from './base.mao';
import * as bcrypt from 'bcrypt';
var nodemailer = require('nodemailer');

export default class UserMAO extends BaseMao {
    user = new Users();
    model = this.user.UserModel;


    // Verify user login attempt
    verifyLogin = (req, res) => {
        var queryItem = { email: req.body.email };
        this.model.findOne(queryItem, (err, user) => {
            if (err) {
                res.status(500).json(err);
            } else if (user) { //if we found a user
                // check the pass
                if (!bcrypt.compareSync(req.body.password, user.password)) {
                    res.status(401).json(err);
                } else {
                    user.password = undefined;
                    res.status(200).json(user);
                }
            } else { //if the is no user
                res.status(401).json(err);
            }
        });
    }

    registerUser = (req, res) => {
        if (req.body.password === req.body.passwordConfirm) {
            var newUser = new this.user.UserModel();
            newUser.email = req.body.email;
            newUser.password = bcrypt.hashSync(req.body.password, 10);
            newUser.created = new Date();
            newUser.active = false;

            // nodemailer.createTestAccount((err, account) => {
            //     // create reusable transporter object using the default SMTP transport
            //     let transporter = nodemailer.createTransport({
            //         host: 'smtp.ethereal.email',
            //         port: 587,
            //         secure: false, // true for 465, false for other ports
            //         auth: {
            //             user: account.user, // generated ethereal user
            //             pass: account.pass  // generated ethereal password
            //         }
            //     });
            //     console.log('sending mail');
            //     let info = transporter.sendMail({
            //         from: '"Life Registry" <LifeReg@noreply.com>', // sender address
            //         to: newUser.email, // list of receivers
            //         subject: "Please validate your email for Life Registry.", // Subject line
            //         text: "Hello! Please validate your email for Life Registry", // plain text body
            //         html: "<b>Hello! Please validate your email for Life Registry</b>" // html body
            //     });
            // });

            newUser.save(function (data) {
                res.status(200).json(data);
            });
        }
    }
}