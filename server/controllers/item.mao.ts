import { Items } from '../models/items';
import BaseMao from './base.mao';

export default class ItemMao extends BaseMao {
    item = new Items();
    model = this.item.ItemModel;

    getAllByReg = (req, res) => {
        console.log(req.body.regName);
        var queryItem = { registry: req.body.registry, userID: req.body.userID };
        this.model.find(queryItem, (err, items) => {
            if (err) {
                console.log(err);
                res.status(500).json(err);
            } else {
                res.status(200).json(items);
            }
        });
    }

    getAllLists = (req, res) => {
        console.log(req.body.regName);
        var queryItem = { userID: req.body.userID };
        this.model.distinct('registry', queryItem, (err, items) => {
            if (err) {
                console.log(err);
                res.status(500).json(err);
            } else {
                res.status(200).json(items);
            }
        });
    }

    saveItem = (req, res) => {
        var itemParams = req.body;
        var newItem = new this.item.ItemModel();
        newItem.title = itemParams.title;
        newItem.url = itemParams.url;
        newItem.description = itemParams.description;
        newItem.registry = itemParams.registry;
        newItem.userID = itemParams.userID;

        newItem.save((err, item) => {
            console.log('saved');
            console.log(item);
            if (err) {
                return res.status(200).json(err);
            } else if (item) {
                return res.status(200).json(item);
            }
        });
    }

    updateItem = (req, res) => {
        var itemParams = req.body;
        console.log(itemParams);
        this.model.update({ _id: itemParams._id }, itemParams, (err, item) => {
            console.log('updated');
            console.log(item);
            if (err) {
                return res.status(200).json(err);
            } else {
                return res.status(200).json(item);
            }
        });
    }

    deleteItem = (req, res) => {
        console.log(req.body);
        var queryItem = { _id: req.body._id };
        this.model.findOneAndDelete(queryItem, (err, item) => {
            console.log('removed');
            console.log(item);
            if (err) {
                return res.status(200).json(err);
            } else {
                return res.status(200).json(item);
            }
        });
    }

}