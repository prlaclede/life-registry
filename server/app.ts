import * as express from 'express';
import * as mongoose from 'mongoose';
import * as logger from "morgan";
import * as bodyParser from "body-parser";
import * as q from "q";
import * as cors from 'cors';
import setRoutes from './routes';

const app = express();

//options for cors midddleware
// const options:cors.CorsOptions = {
//   allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
//   credentials: true,
//   methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
//   origin: "/api",
//   preflightContinue: false
// };

// app.use(cors(options));

// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

app.use(express.json());

app.listen(3000, err => {
  if (err) {
    return console.log(err);
  }
  return console.log('My Express App listening on port 3000');
});

//mount logger
this.app.use(logger("dev"));

app.use(express.urlencoded({ extended: false }));
//mount json form parser
this.app.use(bodyParser.json());

//mount query string parser
this.app.use(bodyParser.urlencoded({
  extended: true
}));

global.Promise = q.Promise;
mongoose.Promise = global.Promise;

// let mongodbURI = 'mongodb+srv://lifereg:l1f3r3g@cluster0-euftp.azure.mongodb.net/test?retryWrites=true&w=majority';
let mongodbURI = 'mongodb+srv://LifeRegApp:Y3ahruCaGmZAl4Yh@cluster0-euftp.azure.mongodb.net/LifeReg?retryWrites=true&w=majority';

mongoose.connect(mongodbURI, { useNewUrlParser: true })
  .then(db => {
    console.log('Connected to MongoDB');
    setRoutes(app, db);
  })
  .catch(err => console.error(err));

export { app };